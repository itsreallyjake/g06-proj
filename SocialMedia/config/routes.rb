Rails.application.routes.draw do
  devise_for :users
  resources :communities, param: :hashtag
  resources :users, param: :email

  get '/' => 'home#index'
  post '/' => 'home#follow_action'
  get '/explore' => 'communities#index'
  get '/explore/new' => 'communities#new'
  post '/explore' => 'communities#create'
  get '/community/:hashtag' => 'communities#show'
  post '/community/:hashtag' => 'communities#follow_action'
  get '/my-profile' => 'users#index'
  post '/my-profile' => 'users#post_action'
  get '/user/:id' => 'users#show'
  post '/user/:id' => 'users#follow_action'
  get '/my-profile/edit' => 'users#edit'
  post '/my-profile/edit' => 'users#update'
  get '/interests' => 'interests#index'
end
