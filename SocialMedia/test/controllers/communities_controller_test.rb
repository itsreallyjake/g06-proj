require 'test_helper'

class CommunitiesControllerTest < ActionDispatch::IntegrationTest
  ############################################################################################################
  # BELOW IS ALL OF THE LOGIN AND SETUP FOR THIS TESTING SUITE #
  #
  # Login Setup (user)
  setup do
    get '/users/sign_in'
    sign_in_as users(:jake)
    post user_session_url
  end

  ############################################################################################################
  # BELOW IS A LIST OF ALL THE TESTS #
  #
  # First Test
  test "Check Explore" do
    get '/explore'
    assert_response(:success)
  end

  # Second Test
  test "Show Community" do
    get '/community/Sports'
    assert_response(:success)
  end

  # Third Test
  test "Community DNE" do
    get '/community/SportsZone'
    assert_redirected_to('/communities')
  end

  # Fourth Test
  test "Check User Signed-in For Explore" do
    delete destroy_user_session_path
    assert_redirected_to("/")
    get '/explore'
    assert_redirected_to("/users/sign_in")
  end

  # Fifth Test
  test "Check User Signed-in For Create" do
    delete destroy_user_session_path
    assert_redirected_to("/")
    get '/explore/new'
    assert_redirected_to("/users/sign_in")
  end

  # Sixth Test
  test "Follow a Community" do
    assert_difference("Follow.count") do
      post '/community/Sports', params: { hashtag: "Sports", type: "follow"}
    end
  end

  # Seventh Test
  test "Follow Without Sign-in" do
    delete destroy_user_session_path
    assert_redirected_to("/")
    post '/community/Sports', params: { hashtag: "Sports"}
    assert_redirected_to("/users/sign_in")
  end

  # Eighth Test
  test "Check Create Community Page if Signed-in" do
    get '/explore/new'
    assert_response(:success)
  end

  # Ninth Test
  test "Create New Community" do
    community = Community.new
    community.name = "New"
    community.hashtag = "NewOne"
    community.description = "This is new"
    community.users_id = users(:jake).id
    assert community.save
  end

  # Tenth Test
  test "Asserts path /communities/Sports returns correct options" do
    assert_recognizes({controller: 'communities', action: 'show', hashtag: 'Sports'}, 'communities/Sports')
  end

  # Eleventh Test
  test "Assert the default action is generated for a route with no action" do
    assert_generates "/communities", controller: "communities", action: "index"
  end

  # Twelvth Test
  test "new action properly routed" do
    assert_generates "/communities/new", controller: "communities", action: "new"
  end

  # Thirteenth Test
  test "Generation of show with parameter" do
    assert_generates "/communities/Sports", {controller: "communities", action: "show", hashtag: "Sports"}
  end

  # Fourteenth Test
  test "Asserts POSTing to /community/:hashtag will call the follow_action" do
    assert_recognizes({controller: 'communities', action: 'follow_action', hashtag: 'Sports'}, {path: 'community/Sports', method: :post})
  end

  # Fifteenth Test
  test "Asserts POSTing to /explore will call the create action" do
    assert_recognizes({controller: 'communities', action: 'create'}, {path: 'explore', method: :post})
  end
end
