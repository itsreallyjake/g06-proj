require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  ############################################################################################################
  # BELOW IS ALL OF THE LOGIN AND SETUP FOR THIS TESTING SUITE #
  #
  # Login Setup (user)
  setup do
    get '/users/sign_in'
    sign_in_as users(:jake)
    post user_session_url
  end

  ############################################################################################################
  # BELOW IS A LIST OF ALL THE TESTS #
  #
  # First Test
  test "Check Index" do
    get '/'
    assert_response(:success)
  end

  # Second Test
  test "Check Redirect to Home After Sign-Out" do
    delete destroy_user_session_path
    assert_redirected_to("/")
  end

  # Third Test
  test "Assert the default action is generated for a route with no action" do
    assert_generates "/", controller: "home", action: "index"
  end

end