require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  ############################################################################################################
  # BELOW IS ALL OF THE LOGIN AND SETUP FOR THIS TESTING SUITE #
  #
  # Login Setup (user)
  setup do
    get '/users/sign_in'
    sign_in_as users(:jake)
    post user_session_url
  end

  ############################################################################################################
  # BELOW IS A LIST OF ALL THE TESTS #
  #
  # First Test
  test "Check Index" do
    get '/my-profile'
    assert_response(:success)
  end

  # Second Test
  test "Check Redirect to Index on Self Parameter" do
    get '/user/1'
    assert_redirected_to("/users")
  end

  # Third Test
  test "Check Show Valid User Success" do
    get '/user/2'
    assert_response(:success)
  end

  # Fourth Test
  test "Asserts POSTing to /my-profile will call the post_action" do
    assert_recognizes({controller: 'users', action: 'post_action'}, {path: 'my-profile', method: :post})
  end

  # Fifth Test
  test "Asserts POSTing to /user/:id will call the follow_action" do
    assert_recognizes({controller: 'users', action: 'follow_action', id: "2"}, {path: 'user/2', method: :post})
  end

  # Sixth Test
  test "Asserts POSTing to /my-profile/edit will call the update" do
    assert_recognizes({controller: 'users', action: 'update'}, {path: 'my-profile/edit', method: :post})
  end

  # Seventh Test
  test "Check User Signed-in For Profile Visit" do
    delete destroy_user_session_path
    assert_redirected_to("/")
    get '/my-profile'
    assert_redirected_to("/users/sign_in")
  end

  # Eighth Test
  test "Check User Signed-in For Other's Profile Visit" do
    delete destroy_user_session_path
    assert_redirected_to("/")
    get '/user/2'
    assert_redirected_to("/users/sign_in")
  end

  # Ninth Test
  test "Create New Post With Hashtag" do
    post = Post.new
    post.post = "This is a new post"
    post.hashtag = "Sports"
    post.user_id = users(:jake).id
    assert post.save
  end

  # Ninth Test
  test "Create New Post Without Hashtag" do
    post = Post.new
    post.post = "This is a new post"
    post.user_id = users(:jake).id
    assert post.save
  end
end