class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    authenticate_user!
    if user_signed_in?
      @user_email = current_user.email.split("@").first
      @user_email[0] = @user_email[0].capitalize
      @user_bio = current_user.bio
      @user_age = current_user.created_at
      @posts = Post.where(user_id: current_user.id)
      @likes = Like.all
    end
  end

  def post_action
    authenticate_user!
    if user_signed_in?
      @newPost = Post.new
      @newPost.post = params[:my_post]
      @newPost.hashtag = params[:hashtag]
      @newPost.user_id = current_user.id
      if @newPost.save
        flash[:alert] = "Your Post Was Sent :)"
        redirect_to action: "index"
      else
        flash[:alert] = "Your Post Failed to Send :("
        redirect_to action: "index"
      end
    end
  end

  def show
    authenticate_user!
    if user_signed_in?
      if current_user.id.to_s == params[:id]
        redirect_to action: "index"
      else
        @follows = Follow.where("to_user = ? AND user_id = ?", params[:id], current_user.id)
        @user = User.find_by(id: params[:id])
        @user_email = @user.email.split("@").first
        @user_email[0] = @user_email[0].capitalize
        @user_bio = @user.bio
        @user_age = @user.created_at
        @posts = Post.where(user_id: @user.id)
        @likes = Like.where(users_id: current_user.id)
      end
    end
  end

  def follow_action
    authenticate_user!
    if user_signed_in?
      if params[:type] == ("follow")
        @newFollow = Follow.new
        @newFollow.user_id = current_user.id
        @newFollow.to_user = params[:id]
        if @newFollow.save
          flash[:alert] = "You Are Now Following This User :)"
          redirect_to action: "show", id: params[:id]
        else
          flash[:alert] = "Your Follow Request Failed :("
          redirect_to action: "show", id: params[:id]
        end

      elsif params[:type] == ("unfollow")
        @follow = Follow.find_by(to_user: params[:id], user_id: current_user.id)
        if @follow.delete
          flash[:alert] = "You Have Unfollowed This User :)"
          redirect_to action: "show", id: params[:id]
        else
          flash[:alert] = "Your Unfollow Request Failed :("
          redirect_to action: "show", id: params[:id]
        end

      elsif params[:type] == ("like")
        @newLike = Like.new
        @newLike.users_id = current_user.id
        @newLike.posts_id = params[:post_id]
        if @newLike.save
          flash[:alert] = "You Liked The Post :)"
          redirect_to action: "show", id: params[:id]
        else
          flash[:alert] = "Your Like Failed :("
          redirect_to action: "show", id: params[:id]
        end

      elsif params[:type] == ("unlike")
        @like = Like.find_by(users_id: current_user.id, posts_id: params[:post_id])
        if @like.delete
          flash[:alert] = "You Unliked The Post :)"
          redirect_to action: "show", id: params[:id]
        else
          flash[:alert] = "Your Unlike Failed :("
          redirect_to action: "show", id: params[:id]
        end
      end
    end
  end

  def like_action
    authenticate_user!
    if user_signed_in?
      if params[:type] == ("like")
        puts params[:post_id]
        @newLike = Like.new
        @newLike.users_id = current_user.id
        @newLike.posts_id = params[:post_id]
        if @newLike.save
          flash[:alert] = "You Liked The Post :)"
          redirect_to action: "show", id: params[:id]
        else
          flash[:alert] = "Your Like Failed :("
          redirect_to action: "show", id: params[:id]
        end
      end
    end
  end

  def edit
    authenticate_user!
  end

  def update
    authenticate_user!
    if user_signed_in?
      @user = User.find_by(id: current_user.id)
      puts user_params
      if @user.update(user_params)
        flash[:alert] = "Your Profile Was Updated :)"
        redirect_to action: "index"
      else
        flash[:alert] = "Your Profile Failed to Update :("
        redirect_to action: "index"
      end
    end
  end

  private
  def post_params
    params.fetch(:post, {}).permit(:my_post, :hashtag)
  end

  def user_params
    params.fetch(:user, {}).permit(:bio)
  end
end