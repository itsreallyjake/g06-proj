class InterestsController < ApplicationController
  def index
    authenticate_user!
    if user_signed_in?
      @user_email = current_user.email.split("@").first
      @user_email[0] = @user_email[0].capitalize

      @followed_users = Follow.joins(:user).where("users.id = follows.user_id")
      @followed_users = @followed_users.where("users.id = ?", current_user.id)
      @count = @followed_users.count
      @followed_users = @followed_users.select(:email, :to_user, :communities_id)
      @user_list = User.all.select(:id, :email)

      @community_list = Community.all.select(:id, :name, :hashtag)

    end
  end
end