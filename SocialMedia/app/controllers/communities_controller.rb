class CommunitiesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    authenticate_user!
    if user_signed_in?
      @community = Community.all
    end
  end

  def new
    authenticate_user!
    if user_signed_in?
      @community = Community.new
    end
  end

  def create
    @community = Community.new
    @community.name = community_params[:name]
    @community.hashtag = community_params[:hashtag]
    @community.description = community_params[:description]
    @community.users_id = current_user.id

    respond_to do |format|
      if @community.save
        format.html { redirect_to @community, notice: 'Community was successfully created.' }
        format.json { render :show, status: :created, location: @community }
      else
        format.html { render :new }
        format.json { render json: @community.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    authenticate_user!
    if user_signed_in?
      @community = Community.find_by_hashtag(params[:hashtag])
      if @community.nil?
        @community = Community.all
        flash[:alert] = "Your community was not found"
        redirect_to action: 'index'
      else
        @community = Community.find_by_hashtag(params[:hashtag])
        @follows = Follow.where("communities_id = ? AND user_id = ?", @community.id, current_user.id)
        @creator = User.find_by(id: @community.users_id)
        @creator = @creator.email
        @creator = @creator.split("@").first
        @creator[0] = @creator[0].capitalize
        @combine = Post.joins(:user).where("users.id = posts.user_id")
        @combine = @combine.where(hashtag: @community.hashtag)
        @combine = @combine.select(:id, :hashtag, :email, :post, :created_at, :user_id)
        @all_likes = Like.all
        @likes = Like.where(users_id: current_user.id)
        @current_id = current_user.id
      end
    end
  end

  def follow_action
    authenticate_user!
    if user_signed_in?
      if params[:type] == ("follow")
        @newFollow = Follow.new
        @newFollow.user_id = current_user.id
        @community = Community.find_by_hashtag(params[:hashtag])
        @newFollow.communities_id = @community.id
        if @newFollow.save
          flash[:alert] = "You Are Now Following This Community :)"
          redirect_to action: "show", id: params[:hashtag]
        else
          flash[:alert] = "Your Follow Request Failed :("
          redirect_to action: "show", id: params[:hashtag]
        end

      elsif params[:type] == ("unfollow")
        @community = Community.find_by_hashtag(params[:hashtag])
        @follow = Follow.find_by(communities_id: @community.id, user_id: current_user.id)
        if @follow.delete
          flash[:alert] = "You Have Unfollowed This Community :)"
          redirect_to action: "show", id: params[:hashtag]
        else
          flash[:alert] = "Your Unfollow Request Failed :("
          redirect_to action: "show", id: params[:hashtag]
        end

      elsif params[:type] == ("like")
        @newLike = Like.new
        @newLike.users_id = current_user.id
        @newLike.posts_id = params[:post_id]
        if @newLike.save
          flash[:alert] = "You Liked The Post :)"
          redirect_to action: "show", id: params[:id]
        else
          flash[:alert] = "Your Like Failed :("
          redirect_to action: "show", id: params[:id]
        end

      elsif params[:type] == ("unlike")
        @like = Like.find_by(users_id: current_user.id, posts_id: params[:post_id])
        if @like.delete
          flash[:alert] = "You Unliked The Post :)"
          redirect_to action: "show", id: params[:id]
        else
          flash[:alert] = "Your Unlike Failed :("
          redirect_to action: "show", id: params[:id]
        end
      end

    end
  end

  private
  def community_params
    params.fetch(:community, {}).permit(:name, :hashtag, :description)
  end

end