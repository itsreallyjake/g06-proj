class HomeController < ApplicationController
  def index
    if user_signed_in?
      @user_email = current_user.email.split("@").first
      @user_email[0] = @user_email[0].capitalize

      @followList = Follow.where("user_id = ?", current_user.id)
      @followList = Post.joins(:follow).where("posts.user_id = follows.to_user AND follows.user_id = ?", current_user.id)

      @combine = Post.joins(:user).where("users.id = posts.user_id")
      @combine = @combine.select(:id, :hashtag, :email, :post, :created_at, :user_id)

      @all_likes = Like.all
      @likes = Like.where(users_id: current_user.id)
      @current_id = current_user.id
    end
  end

  def follow_action
    authenticate_user!
    if user_signed_in?
      if params[:type] == ("like")
        @newLike = Like.new
        @newLike.users_id = current_user.id
        @newLike.posts_id = params[:post_id]
        if @newLike.save
          flash[:alert] = "You Liked The Post :)"
          redirect_to action: "index"
        else
          flash[:alert] = "Your Like Failed :("
          redirect_to action: "index"
        end

      elsif params[:type] == ("unlike")
        @like = Like.find_by(users_id: current_user.id, posts_id: params[:post_id])
        if @like.delete
          flash[:alert] = "You Unliked The Post :)"
          redirect_to action: "index"
        else
          flash[:alert] = "Your Unlike Failed :("
          redirect_to action: "index"
        end
      end
    end
  end
end