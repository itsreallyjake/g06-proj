class FixFkOnLikes < ActiveRecord::Migration[6.0]
  def change
    add_reference :likes, :posts, null: false, foreign_key: true
    remove_column :likes, :communities_id
  end
end
