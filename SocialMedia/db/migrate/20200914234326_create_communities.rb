class CreateCommunities < ActiveRecord::Migration[6.0]
  def change
    create_table :communities do |t|
      t.belongs_to :user
      t.string :name
      t.string :hashtag
      t.text :description
      t.date :creationDate

      t.timestamps
    end
  end
end
