class ChangeName < ActiveRecord::Migration[6.0]
  def change
    rename_column :follows, :users_id, :user_id
  end
end
