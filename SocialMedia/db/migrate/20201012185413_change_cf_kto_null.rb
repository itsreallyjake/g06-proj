class ChangeCfKtoNull < ActiveRecord::Migration[6.0]
  def change
    change_column_null :follows, :communities_id, true
  end
end
