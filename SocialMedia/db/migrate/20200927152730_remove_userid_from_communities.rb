class RemoveUseridFromCommunities < ActiveRecord::Migration[6.0]
  def change
    remove_column :communities, :user_id
  end
end
