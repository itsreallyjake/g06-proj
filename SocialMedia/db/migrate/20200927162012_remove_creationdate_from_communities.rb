class RemoveCreationdateFromCommunities < ActiveRecord::Migration[6.0]
  def change
    remove_column :communities, :creationDate
  end
end
