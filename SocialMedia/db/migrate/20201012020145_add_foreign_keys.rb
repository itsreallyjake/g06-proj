class AddForeignKeys < ActiveRecord::Migration[6.0]
  def change
    add_reference :follows, :users, null: false, foreign_key: true
    add_reference :follows, :communities, null: false, foreign_key: true
  end
end
