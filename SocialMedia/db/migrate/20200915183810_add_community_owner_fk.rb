class AddCommunityOwnerFk < ActiveRecord::Migration[6.0]
  def change
    add_reference :communities, :users, null: false, foreign_key: true
  end
end
